%Calculate the vector potential everywhere

Az=zeros(Nn,1);

Status=zeros(Nn,1);
imj=sqrt(-1);

h=waitbar(0,'Computing the vector potential ...');

for i=1:Ne
    waitbar(i/Ne);
    
    dom=el_mat(i);
    nodes=el_no(i,:);
    
    for k=1:3
        xi=x_no(nodes(k));yi=y_no(nodes(k));
        if Status(nodes(k))==0
            Status(nodes(k))=1;
            % fprintf('Computing of potential: node %d of %d nodes\n',i,Nn);
            for j=1:Nbe
                    n1=boundary_elements(j,1);
                    n2=boundary_elements(j,2);
                    x1=x_no(n1);y1=y_no(n1);
                    x2=x_no(n2);y2=y_no(n2);

                    xc=be_centres_x(j);yc=be_centres_y(j);
                    d=sqrt((xi-xc)^2+(yi-yc)^2);
                    L=be_lengths(j);
                    if dom == 2 || dom == 3 %Inside the Aluminium wire
                        if d<3*L      % Singular integration
                        % if d==0      % Singular integration

                            % Step 1
                            % x1=x_no(n1);y1=y_no(n1);
                            % x2=xi;y2=yi;
                            % num_vel=integral(@Green_sca_2d,0,1);
                            % Ip=num_vel;
                            % Step 2
                            % x1=xi;y1=yi;
                            % x2=x_no(n2);y2=y_no(n2);
                            % num_vel=integral(@Green_sca_2d,0,1);
                            % Ip=Ip+num_vel;
                            
                            Ip1=integral(@Green_K0_2d,0,1)/(2*pi);
                            Ip2=-1/2;
                        else        % Regular integation
                            Ip1=integral(@Green_K0_2d,0,1)/(2*pi);
                            Ip2=-(gamma/(2*pi))*integral(@(x)Green_Dij2_2d(x,nxi,nyi),0,1,'ArrayValued',1);
                        end
                    
                        Az(nodes(k))=Az(nodes(k))+mu2/mu1*Ip1*Current_Density(j+Nbe)-mu2/mu1*Ip2*Current_Density(j);
                    else %Inside Air
                        if d<3*L      % Singular integration
                    
                            Ip1=integral(@Green_es_2d,0,1)/(2*pi);
                            Ip2=-1/2;
                        else        % Regular integation
                            Ip1=integral(@Green_es_2d,0,1)/(2*pi);
                            Ip2=(1/(2*pi))*integral(@(x)Green_Dij1_2d(x,nxi,nyi),0,1,'ArrayValued',1);
                        end
                        f1 = sqrt((xi-Rx)^2+(yi-Ry)^2);
                        Az(nodes(k))=Az(nodes(k))-Ip1*Current_Density(j+Nbe)+Ip2*Current_Density(j)*log(f1)*mu0/(2*pi);
                    end
            end %j
        end % node status
    end %k
    % end % dom==1 (Air nodes)
end %i


close(h);