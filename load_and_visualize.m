% FEM 2-D Scalar Eigenvalue Analysis
% Analysis of a waveguide

addpath('D:\HSR_Forschung\Projekte\HSR\Feldformulationen\Matlab');
load_data;

Px1=100;Px2=600;Py1=100;Py2=380;    % Size of figures
[x_min_bc,y_min_bc,y_max_bc,x_max_bc,R]=find_limits(Nn,x_no,y_no);

if 1 % Geometry and Mesh plot
figure('Position',[Px1 Py1 Px2 Py2],'Color',[1 1 1]);
set_figure_1;
% plot_boundary_edges;
mesh_plot;
geometry_plot;
end

