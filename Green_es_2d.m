function y=Green_es_2d(x)
% x - parameter of the 2d-line-element

global xi yi x1 y1 x2 y2

[p,q]=size(x);
y=zeros(1,q);

L=sqrt((x2-x1)^2+(y2-y1)^2);
for i=1:q

    xt=x1+(x2-x1)*x(i);yt=y1+(y2-y1)*x(i);
    f1=sqrt((xi-xt)^2+(yi-yt)^2);
    y(i)=L*log(f1);

end
return