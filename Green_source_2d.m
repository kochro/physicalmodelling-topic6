function y=Green_source_2d(x)
% x - parameter of the 2d-line-element

global xi yi x1 y1 Rx Ry

%[p,q]=size(x);
%y=zeros(1,q);

%L=sqrt((x2-x1)^2+(y2-y1)^2);
%for i=1:q

    %xt=x1+(Rx-x1)*x(i);yt=y1+(Ry-y1)*x(i);
    %f1=sqrt((xi-Rx)^2+(yi-Ry)^2);
    %y(i)=log(f1);

%end
f1=sqrt((xi-Rx)^2+(yi-Ry)^2);
y = log(f1);
return