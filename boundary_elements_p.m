% Generating boundary mesh

% node_angle=zeros(1,Nn);
% Nbe=128;
% Nbe=0;

if 0
boundary_elements=zeros(Nbe,2);
boundary_element_status=zeros(Nbe,1);
be_centres_x=zeros(Nbe,1);
be_centres_y=zeros(Nbe,1);
be_angles=zeros(Nbe,1);
be_lengths=zeros(Nbe,1);
be_nx=zeros(Nbe,1);
be_ny=zeros(Nbe,1);
be_tx=zeros(Nbe,1);
be_ty=zeros(Nbe,1);
end

node_materials=zeros(Nn,1);

tol=1e-8;

% Centres of three different domains
xc1=-0.02;yc1=0.00; % Potential = +1, boundary_element_status=1 (Electrode 1)
xc2=+0.02;yc2=0.00; % Potential = -1, boundary_element_status=2 (Electrode 2)


ind=0;
for i=1:Nb
        
    n1=bc_elements(i,1);
    n2=bc_elements(i,2);
        
    x1=x_no(n1);y1=y_no(n1);
    x2=x_no(n2);y2=y_no(n2);
    
    xc=(x1+x2)/2;yc=(y1+y2)/2;
    L=sqrt((x2-x1)^2+(y2-y1)^2);
    
    if xc>=-0.03 && xc<=-0.01 && yc>=0 && yc<=0.01 % Electrode 1 (delta Omega 1) 
        ind=ind+1;
        boundary_element_status(ind)=1;
        boundary_elements(ind,1)=n1;boundary_elements(ind,2)=n2;
        be_centres_x(ind)=xc;be_centres_y(ind)=yc;
        be_lengths(ind)=L;
        be_tx(ind)=(x2-x1)/L;be_ty(ind)=(y2-y1)/L;
        r=sqrt((xc-xc1)^2+(yc-yc1)^2);
        r0x=(xc-xc1)/r;r0y=(yc-yc1)/r;
        r0v=[r0x,r0y,0];
        c=cross([be_tx(ind),be_ty(ind),0],[0,0,1]);
        be_nx(ind)=-c(1);be_ny(ind)=-c(2);
    end
    
    if xc>=0.01 && xc<=0.03 && yc>=0 && yc<=0.01 % Electrode 2 (delta Omega 3)
        ind=ind+1;        
        boundary_element_status(ind)=3;
        boundary_elements(ind,1)=n1;boundary_elements(ind,2)=n2;
        be_centres_x(ind)=xc;be_centres_y(ind)=yc;
        be_lengths(ind)=L;
        be_tx(ind)=(x2-x1)/L;be_ty(ind)=(y2-y1)/L;
        r=sqrt((xc-xc2)^2+(yc-yc2)^2);
        r0x=(xc-xc2)/r;r0y=(yc-yc2)/r;
        r0v=[r0x,r0y,0];
        c=cross([be_tx(ind),be_ty(ind),0],[0,0,1]);
        be_nx(ind)=-c(1);be_ny(ind)=-c(2);
    end

        
end % i

Nbe=ind;
