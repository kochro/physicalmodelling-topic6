function y=Green_Dij1_2d(x,nx,ny)
% x - parameter of the 2d-line-element

global xi yi x1 y1 x2 y2 gamma

[p,q]=size(x);
y=zeros(1,q);

L=sqrt((x2-x1)^2+(y2-y1)^2);
n_i = [nx ny];
for i=1:q

    xt=x1+(x2-x1)*x(i);yt=y1+(y2-y1)*x(i);
    f1=sqrt((xi-xt)^2+(yi-yt)^2);
    y(i)=L*besselk(1,gamma*f1)*dot([xi-xt yi-yt],n_i)/f1;

end
return