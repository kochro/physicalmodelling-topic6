% FEM 2-D Scalar Eigenvalue Analysis
% Analysis of a waveguide

addpath('C:\Work\Module\Matlab\Daten_Einlesen\2_D');
addpath('C:\Work\Module\Matlab\graphische_Programme\2_D');
load_data;
% form_edges;

mu=4*pi*1e-7;eps=8.85e-12;
v=1/sqrt(mu*eps);

Px1=100;Px2=600;Py1=100;Py2=380;    % Size of figures
[x_min_bc,y_min_bc,y_max_bc,x_max_bc,R]=find_limits(Nn,x_no,y_no);

if 1 % Geometry and Mesh plot
figure('Position',[Px1 Py1 Px2 Py2],'Color',[1 1 1]);
set_figure_1;
% plot_boundary_edges;
mesh_plot;
plot_boundary_nodes;
end

return;

if 0 % Edge plot
figure('Position',[Px1 Py1 Px2 Py2],'Color',[1 1 1]);
set_figure_1;
mesh_plot;
quiver(cex,cey,ex,ey,0.3,'r');
end

% Matrix Assembly
asm_matrix_wg_ea;

% return;

% Define Boundary Condition
def_bcs_wg_ea;

% Solve the generalized eigenvalue problem [A]{Ez}=kt^2[B]{Ez}
% [Ez,kt_qu]=eigs(A,B,30,'lm');
Nev=20;
opts.v0=zeros(Nn,1);
[Ez,kt_qu]=eigs(A,B,Nev,1000);
% [Ez,kt_qu]=eig(A,B);
fco=zeros(Nev,1);
for i=1:Nev
    fco(i)=sqrt(kt_qu(i,i))*v/(2*pi);
end

% return;

if 1 % Field Plot
mode=1;
fig1=figure('Position',[Px1 Py1 Px2+200 Py2],'Color',[1 1 1]);
axes1 = axes('Parent',fig1,'FontSize',14,'Position',[0.05,0.1,0.7,0.75]);
set_figure_1;
V=Ez(:,mode);
find_min_max;
% max_field=1.3e-8;
% max_field=3;
% min_field=0;
field_plot_filled;
plot_boundary_edges;
title(['TM-Mode, Ez, f_c_o=',num2str(fco(mode)/1e9),'GHz']);
end

