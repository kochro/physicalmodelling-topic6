% Plot boundary elements and nodes
for i=1:Nbe
      %plot(x_no(i),y_no(i),'or');
      if boundary_element_status(i)==0
      line_plot(x_no(boundary_elements(i,1)),y_no(boundary_elements(i,1)),...
          x_no(boundary_elements(i,2)),y_no(boundary_elements(i,2)),'k',1);
      end

      if boundary_element_status(i)==1
      line_plot(x_no(boundary_elements(i,1)),y_no(boundary_elements(i,1)),...
          x_no(boundary_elements(i,2)),y_no(boundary_elements(i,2)),'b',1);
      end

      if boundary_element_status(i)==2
      line_plot(x_no(boundary_elements(i,1)),y_no(boundary_elements(i,1)),...
          x_no(boundary_elements(i,2)),y_no(boundary_elements(i,2)),'r',1);
      end

      if boundary_element_status(i)==3
      line_plot(x_no(boundary_elements(i,1)),y_no(boundary_elements(i,1)),...
          x_no(boundary_elements(i,2)),y_no(boundary_elements(i,2)),'g',1);
      end
            
end

return;

for i=1:Nn
    if st_no(i)>0
        plot(x_no(i),y_no(i),'or');
    end
end
