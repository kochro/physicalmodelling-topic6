%Calculate the vector potential everywhere

Az=zeros(Nn,1);

Status=zeros(Nn,1);
imj=sqrt(-1);

h=waitbar(0,'Computing the vector potential ...');

for i=1:Ne
    waitbar(i/Ne);
    
    dom=el_mat(i);
    nodes=el_no(i,:);
    
    for k=1:3
        xi=x_no(nodes(k));yi=y_no(nodes(k));
        if Status(nodes(k))==0
            Status(nodes(k))=1;
            % fprintf('Computing of potential: node %d of %d nodes\n',i,Nn);
            for j=1:Nbe
                    n1=boundary_elements(j,1);
                    n2=boundary_elements(j,2);
                    x1=x_no(n1);y1=y_no(n1);
                    x2=x_no(n2);y2=y_no(n2);

                    xc=be_centres_x(j);yc=be_centres_y(j);
                    d=sqrt((xi-xc)^2+(yi-yc)^2);
                    L=be_lengths(j);

                    if d<3*L      % Singular integration
                    % if d==0      % Singular integration

                        % Step 1
                        % x1=x_no(n1);y1=y_no(n1);
                        % x2=xi;y2=yi;
                        % num_vel=integral(@Green_sca_2d,0,1);
                        % Ip=num_vel;
                        % Step 2
                        % x1=xi;y1=yi;
                        % x2=x_no(n2);y2=y_no(n2);
                        % num_vel=integral(@Green_sca_2d,0,1);
                        % Ip=Ip+num_vel;

                        Ip=integral(@Green_em_2d,0,1);
                    else        % Regular integation
                        Ip=quad(@Green_em_2d,0,1);
                    end

                    Az(nodes(k))=Az(nodes(k))+imj*omega*mu0*mu1*Ip*Current_Density(j);
            end %j
        end % node status
    end %k
    % end % dom==1 (Air nodes)
end %i

close(h);