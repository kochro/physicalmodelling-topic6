% Load the p-e-t data structure from PDETOOL

clear;
clc;

load mesh_data;

[m,Nn]=size(p); % Nn-number of nodes
[m,Ne]=size(t); % Ne-number of elements

el_no=zeros(Ne,3);                  % Nodes of elements
el_mat=zeros(Ne,1);                 % Material of elements
x_no=zeros(Nn,1);y_no=zeros(Nn,1);  % Nodal coordinates
x_ec=zeros(Ne,1);y_ec=zeros(Ne,1);  % Centers of elements
st_no=zeros(Nn,1);                  % Nodal status (0-domain, 1-boundary)

for i=1:Nn
   x_no(i)=p(1,i);y_no(i)=p(2,i);
   st_no(i)=0;
end

nbe=1;
for i=1:Ne
   el_no(i,1)=t(1,i);
   el_no(i,2)=t(2,i);
   el_no(i,3)=t(3,i);

   x1=x_no(el_no(i,1));x2=x_no(el_no(i,2));x3=x_no(el_no(i,3));
   y1=y_no(el_no(i,1));y2=y_no(el_no(i,2));y3=y_no(el_no(i,3));
   x_ec(i)=(x1+x2+x3)/3;y_ec(i)=(y1+y2+y3)/3;
   
   el_mat(i)=1;
   r=0.01;xc=-0.02;
   if abs(sqrt((x_ec(i)-xc)^2+y_ec(i)^2))<r
       el_mat(i)=2;
   end
   r=0.01;xc=+0.02;
   if abs(sqrt((x_ec(i)-xc)^2+y_ec(i)^2))<r
       el_mat(i)=3;
   end
   
   a=0;
   if y1==y2 && y1==a
       bc_elements(nbe,1)=el_no(i,1);
       bc_elements(nbe,2)=el_no(i,2);
       nbe=nbe+1;
   end
   if y1==y3 && y1==a
       bc_elements(nbe,1)=el_no(i,1);
       bc_elements(nbe,2)=el_no(i,3);
       nbe=nbe+1;
   end
   if y2==y3 && y2==a
       bc_elements(nbe,1)=el_no(i,2);
       bc_elements(nbe,2)=el_no(i,3);
       nbe=nbe+1;
   end

   r=0.1;
   if abs(sqrt(x1^2+y1^2)-r)<1e-4 && abs(sqrt(x2^2+y2^2)-r)<1e-4
       bc_elements(nbe,1)=el_no(i,1);
       bc_elements(nbe,2)=el_no(i,2);
       nbe=nbe+1;
   end
   if abs(sqrt(x1^2+y1^2)-r)<1e-4 && abs(sqrt(x3^2+y3^2)-r)<1e-4
       bc_elements(nbe,1)=el_no(i,1);
       bc_elements(nbe,2)=el_no(i,3);
       nbe=nbe+1;
   end
   if abs(sqrt(x2^2+y2^2)-r)<1e-4 && abs(sqrt(x3^2+y3^2)-r)<1e-4
       bc_elements(nbe,1)=el_no(i,2);
       bc_elements(nbe,2)=el_no(i,3);
       nbe=nbe+1;
   end
   
   r=0.01;xc=-0.02;
   if abs(sqrt((x1-xc)^2+y1^2)-r)<1e-4 && abs(sqrt((x2-xc)^2+y2^2)-r)<1e-4
       bc_elements(nbe,1)=el_no(i,1);
       bc_elements(nbe,2)=el_no(i,2);
       nbe=nbe+1;
   end
   if abs(sqrt((x1-xc)^2+y1^2)-r)<1e-4 && abs(sqrt((x3-xc)^2+y3^2)-r)<1e-4
       bc_elements(nbe,1)=el_no(i,1);
       bc_elements(nbe,2)=el_no(i,3);
       nbe=nbe+1;
   end
   if abs(sqrt((x2-xc)^2+y2^2)-r)<1e-4 && abs(sqrt((x3-xc)^2+y3^2)-r)<1e-4
       bc_elements(nbe,1)=el_no(i,2);
       bc_elements(nbe,2)=el_no(i,3);
       nbe=nbe+1;
   end

   r=0.01;xc=0.02;
   if abs(sqrt((x1-xc)^2+y1^2)-r)<1e-4 && abs(sqrt((x2-xc)^2+y2^2)-r)<1e-4
       bc_elements(nbe,1)=el_no(i,1);
       bc_elements(nbe,2)=el_no(i,2);
       nbe=nbe+1;
   end
   if abs(sqrt((x1-xc)^2+y1^2)-r)<1e-4 && abs(sqrt((x3-xc)^2+y3^2)-r)<1e-4
       bc_elements(nbe,1)=el_no(i,1);
       bc_elements(nbe,2)=el_no(i,3);
       nbe=nbe+1;
   end
   if abs(sqrt((x2-xc)^2+y2^2)-r)<1e-4 && abs(sqrt((x3-xc)^2+y3^2)-r)<1e-4
       bc_elements(nbe,1)=el_no(i,2);
       bc_elements(nbe,2)=el_no(i,3);
       nbe=nbe+1;
   end   
   
end

f1=fopen('nodes.txt','w');
f2=fopen('elements.txt','w');
f3=fopen('domains.txt','w');
f4=fopen('bcs.txt','w');

for i=1:Nn
    fprintf(f1,'%14.6e %14.6e\n\r',x_no(i),y_no(i));   
end

for i=1:Ne
   fprintf(f2,'%d %d %d\n\r',el_no(i,1)-1,el_no(i,2)-1,el_no(i,3)-1);
   fprintf(f3,'%d\n\r',el_mat(i));
end

for i=1:nbe-1
   fprintf(f4,'%d %d\n\r',bc_elements(i,1)-1,bc_elements(i,2)-1);
end

fclose(f1);
fclose(f2);
fclose(f3);
fclose(f4);
