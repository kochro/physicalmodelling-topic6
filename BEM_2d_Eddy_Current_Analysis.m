% BEM 2-D Eddy Current Analysis
% Physical Modelling and Simulation
% Patrik Gjini, Rolf Koch

load_data;

global xi yi x1 y1 x2 y2 nxi nyi k0 Rx Ry gamma

% Parameters definition
% *****************************************************************************************
Nmat=1;                             % Number of different materials
U=1;                                % Voltage between the electrodes
eps0=8.85e-12;                      % Diel. permittivity of vacuum
mu0=4*pi*1e-7;                      % Mag. permeability of vacuum
eps1=1.;                            % Permittivity of air
eps2=4.;                            % Permittivity of epoxi   
sigma=6.3e7;                        % Conductivity of the aluminum (S/m)     
mu1=1.;                             % Permeability of the air
mu2=1.;                             % Permeability of the air     
solver=1;                           % 1 - direct matrix solver; 2 - GMRES iterative solver
Px1=100;Px2=600;Py1=100;Py2=620;    % Size of figures
v=1/sqrt(eps0*mu0*eps1*mu1);        % wave speed
f=100;                              % Plane wave frequency
omega=2*pi*f;                       % Angular frequency
lambda=v/f;                         % Wavelength
k0=2*pi/lambda;                     % Wavenumber
gamma=sqrt(omega*mu0*sigma);        % Gamma

Rx = 0;
Ry = 0;
% *****************************************************************************************

[x_min_bc,y_min_bc,y_max_bc,x_max_bc]=find_boundaries(Nn,x_no,y_no);

% Full mesh of the problem
if 1
    figure('Position', [Px1 Py1 Px2 Py2], 'Color', [1,1,1]);
    set_figure_1;
    geometry_plot;
    mesh_plot;
    title('Domain mesh', 'FontSize',14);
end

boundary_elements_p;

% Geometry of the problem
if 1
    figure('Position',[Px1 Py1 Px2 Py2],'Color',[1 1 1]);
    set_figure_1;
    geometry_plot;
    title('Geometry', 'FontSize',14);
end

% Boundary mesh and normal vectors
if 1
    figure('Position',[Px1 Py1 Px2 Py2],'Color',[1 1 1]);
    set_figure_1;
    plot_boundary_elements;
    quiver(be_centres_x,be_centres_y,be_nx,be_ny,0.8,'k');
    title('Boundary mesh and n-Vectors','FontSize',14);
end

BEM_2d_matrix_assembly;

if 1
% Original Matrix and Residual
Ao=K;R1=norm(abs(b));
fprintf('\n');
fprintf('Residual1 = %14.6e\n',R1);
fprintf('Number of Unknkwns %i\n',Nbe);
end

% Visualisation of the obtained matrix
if 1
    fig1=figure('Position',[100 100 700 720],'Color',[1 1 1]);
    ax1=axes('Parent',fig1,'YDir','reverse');
    contourf(log(abs(K)));
    title('System Matrix','FontSize',14);
    colorbar;
    axis equal;
    set(ax1,'YDir','reverse');
    grid on;
end



switch solver
            % ***************************
    case 1  % direct sparse matrix solver
            % ***************************
        
            V=K\b; % Direct solution
            
            % **********************
    case 2  % GMRES iterative solver
            % **********************
        if 1 % Diagonal Preconditioner
            M1=sparse(Nbe,Nbe);
            M=diag(K);
            for p=1:Nbe
                M1(p,p)=M(p); 
            end
            
            [V,flag,relres,iter,resvec]=gmres(K,b,[],1e-7,Nbe,M1);
            figure('Color',[1,1,1]);
            semilogy(0:iter(2),resvec/norm(b),'-o');
            xlabel('Iteration Number');
            ylabel('Relative Residual');
        end
        
end

% Solution of the overdetermined system by using QR-factorisation

% return;

Current_Density=V;
fprintf('Norm(Charge Vector) %14.6e\n',norm(Current_Density));

R2=norm(abs(Ao*V-b));
fprintf('Residual2 = %14.6e\n',R2);
fprintf('Relative Residual %14.6e\n',R2/R1);

% Charge visualisation
if 1
figure;
%figure('Position',[Px1 Py1 Px2 Py2],'Color',[1 1 1]);
%set_figure_1;
% geometry_plot;
% mesh_plot;
%plot_boundary_elements;

Current_Density_z = zeros(1,Nbe);

for i=1:Nbe
    status=boundary_element_status(i);
    if status == 1
        Current_Density_z(i)=abs(Current_Density(i));
    elseif status == 3
        Current_Density_z(i)=abs(Current_Density(i));
    end
    disp(Current_Density_z(i));
end

quiver3(be_centres_x,be_centres_y, zeros(1,Nbe),zeros(1,Nbe),zeros(1,Nbe),Current_Density_z);
title('abs(Az)','FontSize',14);
end

BEM_Vector_Potential_Ver2;

Az_Real=zeros(1,Nn);
Az_Norm=zeros(1,Nn);

for i=1:Nn
    if real(Az(i))<10e60
        Az_Real(i)=real(Az(i));
    end
    if abs(Az(i))<10e60
        Az_Norm(i)=abs(Az(i));
    end
end

figure;
quiver3(x_no,y_no,zeros(1,Nn),zeros(1,Nn),zeros(1,Nn),Az_Real);
title('vector potential', 'FontSize', 14);
    

clear V;
V=Az_Norm;

figure('Position', [Px1 Py1 Px2 Py2], 'Color', [1 1 1]);

set_figure_1;
find_min_max;
plot_boundary_elements
field_plot_filled;
geometry_plot;
title('Vector Potential (A)', 'FontSize', 14);
    
    
    

% return;
