% BEM 2-D Electrostatic Matrix Assembly
% Equation (42)

N_Mat = Nbe+2;
K=zeros(2*Nbe,2*Nbe);
b=zeros(2*Nbe,1);
imj = sqrt(-1);

for i=1:Nbe
    
    xi=be_centres_x(i);
    yi=be_centres_y(i);
    nxi=be_nx(i);
    nyi=be_ny(i);
    
    status=boundary_element_status(i);
    
    %if status==1 % Electrode 1 (dOmega1)
    %    b(i+Nbe)=*mu0/(2*pi);  % inital Az value
    %end
    %if status==3 % Electrode 2 (dOmega3)
    %    b(i+Nbe)=;  % inital Az value 
    %end
    if status ==1 || status ==3 % (dOmega1 U dOmega3)
               
        for j=1:Nbe

            if i==j % Singular integral
            
                % Two-steps integration (singularity must be at the end of the element)
                % Step 1 Gij
                n1=boundary_elements(j,1);
                n2=boundary_elements(j,2);
                x1=x_no(n1);y1=y_no(n1);
                x2=xi;y2=yi;
                K(i+Nbe,j+Nbe)=integral(@Green_es_2d,0,1)/(2*pi);
                K(i,j+Nbe)= integral(@Green_K0_2d,0,1)/(2*pi);
                % Step 2 Gij
                x1=xi;y1=yi;
                x2=x_no(n2);y2=y_no(n2);
                K(i+Nbe,j+Nbe)=K(i+Nbe,j+Nbe)+integral(@Green_es_2d,0,1)/(2*pi);
                K(i,j+Nbe)=K(i,j+Nbe)+integral(@Green_K0_2d,0,1)/(2*pi);
                %Dij
                K(i,j) = -1/2;
                K(i+Nbe,j) = -1/2;
                % Calculate RHS
                f1=sqrt((xi-Rx)^2+(yi-Ry)^2);
                Green_source = log(f1);
                b(i+Nbe)=Green_source*mu0/(2*pi);
            
            else % Regular integral
    
                n1=boundary_elements(j,1);
                n2=boundary_elements(j,2);
                x1=x_no(n1);y1=y_no(n1);
                x2=x_no(n2);y2=y_no(n2);
                %Dij1
                K(i,j)=(1/(2*pi))*integral(@(x)Green_Dij1_2d(x,nxi,nyi),0,1,'ArrayValued',1);
                %Dij2
                K(i+Nbe,j)=-(gamma/(2*pi))*integral(@(x)Green_Dij2_2d(x,nxi,nyi),0,1,'ArrayValued',1);
                %Gij1
                K(i,j+Nbe)= integral(@Green_K0_2d,0,1)/(2*pi);
                %Gij2
                K(i+Nbe,j+Nbe)=integral(@Green_es_2d,0,1)/(2*pi);
            end % Singular - Regular
        
        end % j
     
    else % (dOmega2)
        disp("This code should never run")
        for j=1:Nbe

            if i==j % Singular integral
            
                K(i,j)=0.5*(eps2+eps1)/(eps2-eps1);
                
            else % Regular integral
    
                n1=boundary_elements(j,1);
                n2=boundary_elements(j,2);
                x1=x_no(n1);y1=y_no(n1);
                x2=x_no(n2);y2=y_no(n2);
                K(i,j)=-1/(2*pi)*integral(@dn_Green_em_2d,0,1); 
                
            end % Singular - Regular
        
        end % j        
        
    end % Kind of boundary
    
end

